const CITAS = [
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "08:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "9:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "10:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "11:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "12:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "13:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "14:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "15:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "16:00",
    },
    {
        especialidad: "odontología",
        nombre: "Dr. Juan",
        fecha: "2020-01-01",
        hora: "17:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "08:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "9:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "10:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "11:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "12:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "13:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "14:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "15:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "16:00",
    },
    {
        especialidad: "medicina",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "17:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "8:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "9:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "10:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "11:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "12:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "13:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "14:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "15:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "16:00",
    },
    {
        especialidad: "optometría",
        nombre: "Dr. Mario",
        fecha: "2020-01-01",
        hora: "16:00" 
    }
]

const obtenercitasDisponibles=(especialidad,fecha_inicio,fecha_final)=>{
	return CITAS.filter(element=> (
        especialidad === element.especialidad 
        &&
        element.fecha >= fecha_inicio 
        &&
        element.fecha <= fecha_final
        ));	
}

const obtenerCitaPorJornada = (jornadaPreferia)=>{
    return CITAS.filter(element=> (
        element.hora.split(":") //["16", "00"]
    ));
}